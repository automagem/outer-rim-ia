var $ = require('jquery')
var _ = require('lodash')
require('popper.js/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')

var cssify = require('cssify')
cssify.byUrl('./bootstrap.min.css')

var character = ['Aphra', 'Boba', 'Bossk', 'IG88', 'Jyn', 'Ketsu', 'Lando', 'Solo']
var iaDeck = 'Aphra'

$(document).ready(function () {
  // create a list with characters:
  $('#IaList').append('<ul class="list-group"></ul>')
  _.forEach(character, function (name) {
    var nameText = '<h1>' + name + '</h1>'
    $('.list-group').append('<li class="list-group-item">' + nameText + '</li>')
  })

  // select ia character
  $('.list-group li').on('click', function () {
    iaDeck = this.textContent
    automaDeck(this.textContent)
  })

  function automaDeck (name) {
    var card0url = './data/' + name + '/0.jpg'
    $('#cardcontainer').html('<img class="img-fluid" id="card" src="' + card0url + '">')
    // deal new card each time the user makes a click
    $('#card').on('click', function () {
      var randomCardId = _.random(1, 10)
      var newCard = './data/' + name + '/' + randomCardId + '.jpg'
      console.log(newCard)

      $('#card').attr('src', newCard)
    })
  }
})
