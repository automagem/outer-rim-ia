0# Outer Rim AI card decks

This proyect is a HTML implementation of the AI card decks by alastair john jack (BGG)

Original Files:

https://boardgamegeek.com/filepage/195147/complete-ai-card-decks-each-character-based-game-g

## Getting started

```
git clone https://gitlab.com/automagem/outer-rim-ia.git
cd outer-rim-ia/public/src
npm install
npm run built
npm run watch
// start local server
npm run server
```
